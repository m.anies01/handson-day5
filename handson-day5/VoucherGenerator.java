public class VoucherGenerator{
    static String getRandomCode(int jmlKarakter){
        String getRandomCode = "abcdefghijklmnopqrstuvxyz";
        StringBuilder rndCode = new StringBuilder(jmlKarakter);

        for (int i = 0; i < jmlKarakter; i++) {
            int index = (int)(getRandomCode.length() * Math.random());
            rndCode.append(getRandomCode.charAt(index));
        }
        return rndCode.toString();
    }
    public static void main(String[] args) {
                int jmlKarakter = 10;

                for(int i = 0; i < 10; i++){
                    System.out.println("Voucher Kode yang berlaku: "+VoucherGenerator.getRandomCode(jmlKarakter));
                }
    }
}